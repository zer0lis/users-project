# AngularJS codebase


## Stack

* Latest version of [AngularJS](https://angularjs.org);
* [Gulp](https://gulpjs.com) for building;
* Folders-by-Feature structure;
* Component-based design;
* [AngularUI Router](http://angular-ui.github.io/ui-router/) for client-side routing;
* Data calls through services;
* ECMAScript 6 (arrow functions, strings interpolation);
* [Browsersync](https://browsersync.io/);

## How to use

1. clone or download
2. `yarn` or `npm i`
3. `gulp build` or `gulp` to serve the app in dev mode
4. configure app.settings.js