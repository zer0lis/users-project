// app.module.js
(() => {

    angular.module('app', ['ui.router', 'ngStorage', 'ngMaterial', 'ngMessages', 'ngMdIcons'])
        .run(function ($localStorage, $http) {
            if($localStorage.currentUser) {
                // add jwt token to auth header for all requests made by the $http service
                $http.defaults.headers.common.Authorization = 'Token ' + $localStorage.currentUser.token;
            }

        })

})();