// home.controller.js
(() => {

    angular
        .module('app')
        .controller('HomeController', HomeController);

    function HomeController($localStorage, UserService) {
        const vm = this;
        vm.header = 'You are not logged in';
        vm.$onInit = onInit;
        vm.isLoggedIn = true;
        vm.info = false;
        vm.showEditForm = false;
        vm.edit = edit;

        activate();

        ////////////

        function activate() {
            // Resolve start-up logic


        }

        function onInit() {
            vm.info = UserService.GetUser();

            if(vm.info) {
                const role = vm.info.roles.indexOf(1) !== -1 ? 'Admin' : 'User';
                vm.header = `Welcome ${role}: ${$localStorage.currentUser.username}. It is your ${$localStorage.currentUser.loginCount} visit here`
            }
        }

        function edit() {
            vm.loading = true;
            const data = {
                id: vm.info.id,
                bio: vm.bio
            };
            UserService.EditUser(data, function (result) {
                if (result === true) {
                    vm.info = UserService.GetUser();
                    vm.loading = false;
                } else {
                    vm.error = `Something went wrong: ${result}`;
                    vm.loading = false;
                }
            });
        }
    }

})();