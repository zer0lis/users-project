
(() => {

    angular
        .module('app')
        .controller('LoginController', LoginController);

    function LoginController($location, UserService) {
        const vm = this;
        vm.login = login;
        vm.$onInit = onInit;

        activate();

        ////////////

        function activate() {
            // Resolve start-up logic
            UserService.Logout();
        }

        function onInit() {
            // Initialization logic that relies on bindings being present
            // should be put in this method, which is guarranteed to
            // always be called after the bindings have been assigned.
        }

        function login() {
            vm.loading = true;
            UserService.Login(vm.username, vm.password, function (result) {
                if (result === true) {
                    $location.path('/');
                } else {
                    vm.error = 'Username or password is incorrect';
                    vm.loading = false;
                }
            });
        }
    }

})();