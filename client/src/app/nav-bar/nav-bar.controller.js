// home.controller.js
(() => {

    angular
        .module('app')
        .controller('NavBarController', NavBarController);

    function NavBarController(UserService) {
        const vm = this;
        vm.$onInit = onInit;
        vm.isAdmin = false;
        activate();

        ////////////

        function activate() {
            // Resolve start-up logic

        }
        
        function onInit() {
            // Initialization logic that relies on bindings being present
            // should be put in this method, which is guarranteed to
            // always be called after the bindings have been assigned.
            const loggedUser = UserService.GetUser();

            if(loggedUser) {
                if(loggedUser.roles.includes(1)) vm.isAdmin = true;
            }
        }
    }

})();