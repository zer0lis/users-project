// register.component.js
(() => {

    angular
        .module('app')
        .component('register', {
            controller: 'RegisterController',
            controllerAs: 'vm',
            templateUrl: 'app/register/register.html'
        });

})();