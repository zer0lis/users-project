
(() => {

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    function RegisterController($location, UserService) {
        const vm = this;
        vm.register = register;
        vm.$onInit = onInit;

        activate();

        ////////////

        function activate() {
            // Resolve start-up logic
        }

        function onInit() {
            // Initialization logic that relies on bindings being present
            // should be put in this method, which is guarranteed to
            // always be called after the bindings have been assigned.
        }

        function register() {
            vm.loading = true;
            UserService.RegisterUser(vm.username, vm.password, function (result) {
                if (result === true) {
                    $location.path('/users');
                } else {
                    vm.error = 'Username or password is incorrect';
                    vm.loading = false;
                }
            });
        }
    }

})();