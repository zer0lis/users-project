// api.service.js
(() => {

    angular
        .module('app')
        .factory('apiService', ApiService);

    function ApiService(apiBase, $http) {

        return {
            api: api
        };

        function api(endpoint, method, data) {
            return $http({
                        method: method,
                        url: `${apiBase}${endpoint}`,
                        data: data,
                        cache: false
                        })
                    .then(response => response.data);
        }

    }

})();