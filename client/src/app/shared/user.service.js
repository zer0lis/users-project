(() => {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    function UserService(apiService, $localStorage, $http) {

        return {
            Login: Login,
            Logout: Logout,
            GetUser: GetUser,
            RegisterUser: RegisterUser,
            EditUser: EditUser,
            DeleteUser: DeleteUser,
            GetUsers: GetUsers
        };

        function Login(username, password, callback) {
            return apiService
                .api('users/login', 'POST', {"user": {email: username, password: password}}, true)
                .then(function successCallback(response) {
                // login successful if there's a token in the response
                if (response.user.token) {

                    // store username and token in local storage to keep user logged in between page refreshes
                    $localStorage.currentUser = {
                        id: response.user.id,
                        username: username,
                        token: response.user.token,
                        bio: response.user.bio,
                        loginCount: response.user.loginCount,
                        roles: response.user.roles
                    };

                    // add jwt token to auth header for all requests made by the $http service
                    $http.defaults.headers.common.Authorization = 'Token ' + response.user.token;

                    // execute callback with true to indicate successful login
                    callback(true);
                } else {
                    // execute callback with false to indicate failed login
                    callback(false);
                }
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                    callback(false);
                    console.error(response);
            });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }

        function GetUser() {
            return $localStorage.currentUser ? $localStorage.currentUser : false;
        }

        function EditUser(data, callback) {
            if(!data) callback(false);

            return apiService
                .api('user', 'PUT', {"user": data}, true)
                .then(function successCallback(response) {
                    if (response) {
                        callback(true);
                    }

                }, function errorCallback(response) {
                    callback(false);
                    console.error(response);
                });
        }

        function DeleteUser(email, callback) {
            if(!email) callback(false);

            return apiService
                .api('users/' + email, 'DELETE', false)
                .then(function successCallback(response) {
                    if (response) {
                        callback(response);
                    }

                }, function errorCallback(response) {
                    callback(false);
                    console.error(response);
                });
        }

        function RegisterUser(username, password, callback) {
            if(!username || !password) callback(false);

            return apiService
                .api('users', 'POST', {"user": {email: username, password: password}}, true)
                .then(function successCallback(response) {
                    if (response) {
                        callback(true);
                    }
                }, function errorCallback(response) {
                    callback(false);
                    console.error(response);
                });
        }

        function GetUsers(callback) {
            return apiService
                .api('users', 'get', '', true)
                .then(function successCallback(response) {
                    if (response) {
                        callback(response);
                    }
                }, function errorCallback(response) {
                    callback(false);
                    console.error(response);
                });
        }
    }
})();
