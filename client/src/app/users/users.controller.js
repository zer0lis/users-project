// users.controller.js
(() => {

    angular
        .module('app')
        .controller('UsersController', UsersController);

    function UsersController(UserService, $mdDialog) {
        const vm = this;
        // $transitions.onStart({},function() {
        //     onInit();
        // });
        vm.$onInit = onInit;
        vm.users = null;
        vm.header = 'Users';
        vm.deleteUser = deleteUser;
        vm.editUser = editUser;
        vm.getUsers = getUsers;

        activate();

        ////////////

        function activate() {
            // // Resolve start-up logic
            // UserService.GetUsers(function (result) {
            //     if (!!result) {
            //         vm.users = result;
            //         console.log('vm.users', vm.users)
            //     } else {
            //         throw Error('Something went wrong with the request');
            //     }
            // });
        }

        function onInit() {
            UserService.GetUsers(function (result) {
                if (!!result) {
                    vm.users = result;
                } else {
                    throw Error('Something went wrong with the request');
                }
            });
        }

        function deleteUser(user) {
            const confirm = $mdDialog.confirm()
                .title('Are you sure?')
                .textContent('All of the banks have agreed to forgive you your debts.')
                .ok('OK!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                UserService.DeleteUser(user.user.email, function (result) {
                    if(result) vm.getUsers();
                });
            }, function() {
            });
        }

        function editUser(user) {
            const confirm = $mdDialog.prompt()
                .title('Edit User\'s bio')
                .placeholder('New user bio')
                .initialValue(user.user.bio)
                .ok('OK!')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function(result) {
                const data = {
                    id: user.user.id,
                    bio: result
                };
                UserService.EditUser(data, function (result) {
                    if(result) vm.getUsers();
                });
            }, function() {
            });
        }

        function getUsers() {
            return UserService.GetUsers(function (result) {
                if (!!result) {
                    vm.users = result;
                } else {
                    throw Error('Something went wrong with the request');
                }
            });
        }
    }

})();