
> ### NestJS codebase containing user CRUD, auth, advanced patterns, login count, etc,

## Installation
    npm install

Copy config file and set JsonWebToken secret key

    cp configs/config.ts.example configs/config.ts
    
----------

## Database

The example codebase uses [Typeorm](http://typeorm.io/) with a postgreSQL database.

Create a new mysql database with the name `name` (or the name you specified in the ormconfig.json)

Copy Typeorm config example file for database settings

    cp ormconfig.json.example ``
    
Set mysql database settings in ormconfig.json

    {
      "type": "postgres",
      "host": "localhost",
      "port": 5432,
      "username": "your-sql-username",
      "password": "your-sql-password",
      "database": "dbname",
      "entities": ["src/**/**.entity{.ts,.js}"],
      "synchronize": true
    }
    
Start local sql server and create new database 'dbname'

On application start, tables for all entities will be created.

----------

## NPM scripts

- `npm start` - Start application
- `npm run start:watch` - Start application in watch mode
- `npm run test` - run Jest test runner. Configured but not actual tests implemented 
- `npm run start:prod` - Build application
- Test api with `http://localhost:3000/api/` in your favourite browser

----------

# Authentication
 
This applications uses JSON Web Token (JWT) to handle authentication. The token is passed with each request using the `Authorization` header with `Token` scheme. 
The JWT authentication middleware handles the validation and authentication of the token.

----------
 
# Swagger API docs

 `http://localhost:3000/docs/`
 
NestJS swagger module for API documentation. [NestJS Swagger](https://github.com/nestjs/swagger) - [www.swagger.io](https://swagger.io/)        