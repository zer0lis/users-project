export class UpdateUserDto {
  readonly id: number;
  readonly email?: string;
  readonly bio?: string;
  readonly loginCount?: number;

}
