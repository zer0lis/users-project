export interface UserData {
  email: string;
  token: string;
  bio: string;
  loginCount: number;
  roles: Array<number>;
}

export interface UserRO {
  user: UserData;
}
